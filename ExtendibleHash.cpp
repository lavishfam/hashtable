#include "ExtendibleHash.h"
#include <cmath>

int Ehash(int value, int bits)
{
  return value >> (18 - bits) ;
} // Ehash()

ExtendibleHash::ExtendibleHash(const int & notFound, int b, int LSize) :
  bits(b), LeafSize(LSize)
{
  //build hash table with bit&size
  size = pow(2,b);
  directory = new ExtendibleLeaf*[size];

  ExtendibleLeaf *child;
  child = new ExtendibleLeaf(LeafSize, b); //making an initial leaf child
  
  for(int i = 0; i < size; i++)
    directory[i] = child; //makes all directory nodes point to initial child
} // ExtendibleHash()



void ExtendibleHash::remove(const int &object)
{
  directory[Ehash(object,bits)]->removeData(object);
}  // remove()


const int & ExtendibleHash::find(const int &object)
{
  int insert_location = Ehash(object,bits);
  return  directory[insert_location]->findData(object);
}  // find()

const int & ExtendibleLeaf::findData(const int &object)
{
  int const &temp = -1;
  for(int i = 0; i < count; i++)
  {
    if(data[i] == object)
    {
      return data[i];
    }
  }
  return temp;
}




void ExtendibleHash::splitOrRehash(const int &object)
{
  int insert_location = Ehash(object,bits);
  //cout << " insert location is " << insert_location << endl;
  int index = insert_location;
  int newSize;

  ExtendibleLeaf **newDirectory;
  ExtendibleLeaf * new_leaf = new ExtendibleLeaf(LeafSize, bits);

  //cout << " INDEX MY NIGGUH IS "  << index << endl;
  int key = Ehash(directory[index]->data[0],bits);
  int key2, reference;  
  int doesNotMatch = 0;

  for(int i = 0; i < LeafSize; i++){
    key2 = Ehash(directory[index]->data[i], bits);
    //cout << "ELEMENT IS  " << directory[index]->data[i] << endl;
    if(key != key2){
      /*cout << " KEY ONE IS " << key << "KEY 2 IS " << key2 << endl;
      cout << "BITS ARE " << bits << endl;
      cout << " it doesn't match" << endl;*/
      doesNotMatch++;
    }
  }

if(doesNotMatch > 0){
  for(int i = 1; i < LeafSize; i++)
  {
    //cout << "data[0] is " << directory[index]->data[0] << endl;
    //cout << " data[i] is " << directory[index]->data[i] << endl;
    //cout << " bits is " << bits << endl;
    key2 = Ehash(directory[index]->data[i], bits);
//    cout << "key 1 is " << key << "and key2 is " << key2 << endl;
    if(key != key2)
    { //new leaf child to be inserted into
      reference = key2;
      //cout << " now transferring" << endl;
      //new_leaf->insertData(directory[index]->data[i]);
      //cout << " now removing" << endl;
      //directory[index]->removeData(directory[index]->data[i]);
      //cout << " my ass was split with " << object << endl;
      doesNotMatch++;
      for(int j = i; j < directory[index]->getCount(); j++)
      {
        
        if(reference == Ehash(directory[index]->data[j],bits))
        { //cout<<"START TRANSFERRING" << endl;
          new_leaf->insertData(directory[index]->data[j]);
          directory[index]->removeData(directory[index]->data[j]);
          j--;
          //cout<<"STOP TRANSFERRING"<<endl;
        }
      }
      directory[index] = new_leaf; //FUCK YES HANH THIS WAS THE KEY
      this->insert(object);
      break;
    
      //this->removeData(data[i]);
    }
  }//split
  //cout << " object before rehash check" << object <<  endl;
}
  if(doesNotMatch == 0){
    newSize = size * 2;
    /*cout << " size is " << size << " for bits " << bits <<  endl;
    cout << "REFUCKING AHDSFKJSADLKFJAS;LKFJSDFJSDJKF0 " << endl*/;
    newDirectory = new ExtendibleLeaf*[size];
    for(int i = 0; i < newSize; i++)
        {
      if(i < size){
        newDirectory[i] = directory[i]; //match the children to new directory
        }
      else
	{
        newDirectory[i] = directory[index];//for the rest of the directory, 
        }/*
        //set it equal to what caused the rehash
        int j = i*2;
        if(i == 0)
        {
	  newDirectory[j] = directory[i];
          newDirectory[j+1]=directory[i];
	}
        else
	{
          newDirectory[j] = directory[i];
          newDirectory[j-1]=directory[i];
        }*/
    }
    directory = newDirectory;
    size = newSize;
    bits = bits*2;
    for(int k = 0; k < size; k++)
      (directory[k]->bits)=bits;
    //cout << " BITS HAS BEEN CHANGED: NOW " << bits << endl;
   // cout << " went into rehash function, before successful insert" << endl;
    //cout << "OBJECT BEFORE INSERT" << object << endl;
    this->insert(object);
    //cout << "The directory was rehashed at " << object << endl;

  }//rehash






    //directory[insert_location]->splitData(object);

  //directory[insert_location]->insertData(object);
}  // split()






void ExtendibleHash::insert(const int&object)
{
 // cout << "object is " << object << endl;
  int insert_location = Ehash(object,bits);
 // cout << " insert location is " << insert_location << endl;
  //cout << " count of current is " << directory[insert_location]->getCount() << endl;
  //cout << "ass " << endl;
  if(directory[insert_location]->getCount() == LeafSize){
    //cout << "object is now" << object << endl;
     this->splitOrRehash(object);//if its full, check if you need to split or rehash
   }
  else{
   directory[insert_location]->insertData(object);
 }
       
} // insert()




ExtendibleLeaf::ExtendibleLeaf(int LSize, int bit):LeafSize(LSize), bits(bit)
{
  count = 0;
  data = new int[LSize];
}//ExtendibleLeaf()


int ExtendibleLeaf::getCount()
{
  return count;
}



void ExtendibleLeaf::insertData(const int &object)
{
//  int temp = object; //puts object into an int so it isnt a reference
  data[count]= object;
  //cout << data[count] << " was inserted into my asshole." << endl;
  count++;
  //cout << " number of bits are" << bits << endl;
  //cout << " and leaf count is " << count << endl;
}

void ExtendibleLeaf::removeData(const int &object)
{
  //cout << object << "was removed from my asshole." << endl << "bits are "<< bits<<endl;
  for(int i =0; i < count; i++)
    if(data[i] == object)
    {
      if(i == (count-1))
      {
        data[count-1]=0;
        count--;
      }
      else
      {
        data[i] = data[count-1];
        data[count-1] = 0;
        count--;
      }
    }
}

/*ExtendibleLeaf* ExtendibleLeaf::splitData(const int &object){
    int insert_location = Ehash(object,bits);
    ExtendibleLeaf * new_leaf;
    new_leaf = new ExtendibleLeaf(LeafSize, bits);
    //cout << "HELLO " << endl;
    
    int key = Ehash(this->data[0],bits) ;
    int key2; 
    //take first element, find next element that doesnt match, and create new child
    
    int doesNotMatch = 0;
    for(int i = 1; i < LeafSize; i++)
    {
      key2 = Ehash(this->data[i], bits);
      if(key != key2){ //new leaf child to be inserted into
        new_leaf->insertData(this->data[i]);
        doesNotMatch++;
        //this->removeData(data[i]);
      }
    }
    if(doesNotMatch == 0){
      cout << "I NEED TO REHASH" << endl;
    }

    return new_leaf;
    
}*/
