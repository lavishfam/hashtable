#ifndef EXTENDIBLE_HASH_H
  #define EXTENDIBLE_HASH_H
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <cstdlib>
using namespace std;

class ExtendibleLeaf
{

friend class ExtendibleHash;
private:
  int *data;
  int count;
  int LeafSize;
  int bits;

public:
  ExtendibleLeaf(int LSize, int bit);
  int getCount() ;
  void removeData(const int & object);
  void insertData(const int & object);
  ExtendibleLeaf* splitData(const int & object);
  const int & findData(const int & object);
};

class ExtendibleHash
{


private:
  ExtendibleLeaf  **directory;
  int bits;
  int size;
  int LeafSize;

public:
  ExtendibleHash(const int & notFound, int s, int LSize = 2);
  void insert(const int &object);
  void remove(const int &object);
  const int & find(const int &object);
  void splitOrRehash(const int &object);
  int GetBits()const {return bits;}


}; // class ExtendibleHashing



#include "ExtendibleHash.cpp"

#endif

